# Offline Puzzle Solver

An offline puzzle solver covering:

+ 3/N-cups problem (with varying amounts of cups)
+ 8/N-Queens (up to 32x32)
+ Sudoku
+ The NetGuard firewall's Challenge (Note: Offline Puzzle Solver is not developed by any NetGuard developers and uses no NetGuard code. Functionality may break at any time, especially if NetGuard updates.)

## License
gre
NOTE: This license will only cover the Gradle Wrapper files (`gradlew`, `gradlew.bat`, `gradle/*`) if they are in fact compatible.

```
    Offline Puzzle Solver
    Copyright (C) 2021 Contributors To The Offline Puzzle Solver

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

## Credits

For full credits (including dependencies), see `app/src/main/assets/credits.html`.

## PR Notes

+ PRs do NOT require copyright assignment. However, they do require you to sign the credits file if you haven't already to ensure you are properly credited.

