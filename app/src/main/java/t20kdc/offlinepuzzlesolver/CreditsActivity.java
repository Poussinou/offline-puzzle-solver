package t20kdc.offlinepuzzlesolver;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

/**
 * The end.
 */
public class CreditsActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credits);
        ((WebView) findViewById(R.id.webview)).loadUrl("file:///android_asset/credits.html");
    }
}