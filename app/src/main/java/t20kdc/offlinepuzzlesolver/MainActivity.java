package t20kdc.offlinepuzzlesolver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;

import t20kdc.offlinepuzzlesolver.game.NQueensActivity;
import t20kdc.offlinepuzzlesolver.game.NCupsActivity;
import t20kdc.offlinepuzzlesolver.game.SudokuActivity;
import t20kdc.offlinepuzzlesolver.ungame.NetGuardChallengeActivity;

/**
 * The beginning.
 */
public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.btn_start_three_cups).setOnClickListener((x) -> {
            startActivity(new Intent(MainActivity.this, NCupsActivity.class));
        });
        findViewById(R.id.btn_start_n_queens).setOnClickListener((x) -> {
            startActivity(new Intent(MainActivity.this, NQueensActivity.class));
        });
        findViewById(R.id.btn_start_sudoku).setOnClickListener((x) -> {
            startActivity(new Intent(MainActivity.this, SudokuActivity.class));
        });
        findViewById(R.id.btn_netguard_challenge).setOnClickListener((x) -> {
            startActivity(new Intent(MainActivity.this, NetGuardChallengeActivity.class));
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // is a thing, does a stuff
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        // return true for something handled
        if (id == R.id.action_credits) {
            startActivity(new Intent(MainActivity.this, CreditsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}