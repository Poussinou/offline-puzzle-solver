package t20kdc.offlinepuzzlesolver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

public class SolutionActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solution);
        String str = getIntent().getStringExtra(Intent.EXTRA_TITLE);
        if (str != null)
            setTitle(str);
        String pipe = getIntent().getStringExtra("t20kdc.top.pipe");
        WebView wv = findViewById(R.id.webview);
        wv.getSettings().setBuiltInZoomControls(true);
        if (pipe != null) {
            wv.loadUrl(pipe);
        } else {
            String data = getIntent().getStringExtra(Intent.EXTRA_HTML_TEXT);
            if (data == null)
                data = "<h1>The solution was lost in intent transit.</h1>";
            wv.loadDataWithBaseURL("file:///android_asset/", data, "text/html", "UTF-8", "");
        }
    }
}