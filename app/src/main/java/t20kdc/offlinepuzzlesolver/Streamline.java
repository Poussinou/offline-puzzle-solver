package t20kdc.offlinepuzzlesolver;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

public class Streamline {
    private final Bundle bundle;
    private final SharedPreferences.Editor prefsEditor;
    private final boolean forcePrefsReader;
    private final SharedPreferences prefsReader;
    public final boolean saving;
    public final Activity context;

    public Streamline(Activity context, Bundle bundle, boolean saving) {
        this.context = context;
        // Due to Android N stupidity, we can't make preferences world-readable & world-writable.
        // This severely annoys me, but we'll have to deal with it.
        prefsReader = context.getPreferences(0);
        if (saving) {
            prefsEditor = prefsReader.edit();
        } else {
            prefsEditor = null;
        }
        if (saving)
            if (bundle != null)
                bundle.putBoolean("streamline_valid", true);
        forcePrefsReader = (bundle == null) || !bundle.containsKey("streamline_valid");
        this.bundle = bundle;
        this.saving = saving;
    }

    public static void bindSeekWdg(Activity act, int seek_cup_move, int seek_cup_move_t, int base, Runnable update) {
        SeekBar sb1 = (SeekBar) act.findViewById(seek_cup_move);
        TextView sb2 = (TextView) act.findViewById(seek_cup_move_t);
        SeekBar.OnSeekBarChangeListener sc = new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                // everything I do here is considered wrong
                sb2.setText(Integer.toString(sb1.getProgress() + base));
                if (update != null)
                    update.run();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        };
        sc.onProgressChanged(null, 0, false);
        sb1.setOnSeekBarChangeListener(sc);
    }

    public void putString(String id, String value) {
        if (bundle != null)
            bundle.putString(id, value);
        if (prefsEditor != null)
            prefsEditor.putString(id, value);
    }

    public void putInt(String id, int value) {
        if (bundle != null)
            bundle.putInt(id, value);
        if (prefsEditor != null)
            prefsEditor.putInt(id, value);
    }

    public String getString(String id, String b) {
        if (!forcePrefsReader)
            return bundle.getString(id, b);
        return prefsReader.getString(id, b);
    }

    public int getInt(String id, int b) {
        if (!forcePrefsReader)
            return bundle.getInt(id, b);
        return prefsReader.getInt(id, b);
    }

    public void interact(String id, int aid, Object def) {
        View v = context.findViewById(aid);
        interact(id, v, def);
    }
    public void interact(String id, View v, Object def) {
        try {
            if (v instanceof EditText) {
                if (saving) {
                    putString(id, ((EditText) v).getText().toString());
                } else {
                    ((EditText) v).setText(getString(id, (String) def));
                }
            } else if (v instanceof CheckBox) {
                if (saving) {
                    putInt(id, ((CheckBox) v).isChecked() ? 1 : 0);
                } else {
                    ((CheckBox) v).setChecked(getInt(id, ((boolean) (Boolean) def) ? 1 : 0) != 0);
                }
            } else if (v instanceof SeekBar) {
                if (saving) {
                    putInt(id, ((SeekBar) v).getProgress());
                } else {
                    ((SeekBar) v).setProgress(getInt(id, (Integer) def));
                }
            }
        } catch (Exception ex) {
            // catches preference type errors
            ex.printStackTrace();
        }
    }

    public void finish() {
        if (prefsEditor != null)
            prefsEditor.apply();
    }
}
