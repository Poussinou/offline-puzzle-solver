package t20kdc.offlinepuzzlesolver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;

import t20kdc.offlinepuzzlesolver.game.Solver;

public abstract class StreamlineActivity extends Activity {
    public final void loadSL(Bundle savedInstanceState) {
        Streamline sl = new Streamline(this, savedInstanceState, false);
        onStreamline(sl);
        sl.finish();
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Streamline sl = new Streamline(this, savedInstanceState, true);
        onStreamline(sl);
        sl.finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Streamline sl = new Streamline(this, null, true);
        onStreamline(sl);
        sl.finish();
    }

    public abstract void onStreamline(Streamline streamline);

    public void bindSolveButton(Class<?> cs) {
        Log.println(Log.INFO, "StreamlineActivity", "Solve button binding (class " + cs.toString() + ")");
        findViewById(R.id.btn_solve).setOnClickListener((b) -> {
            Log.println(Log.INFO, "StreamlineActivity", "Solve button pressed (class " + cs.toString() + ")");
            final Bundle bundle = new Bundle();
            Streamline sl = new Streamline(this, bundle, true);
            onStreamline(sl);
            sl.finish();
            WorkingActivity.launch(this, new Thread("PuzzleSolverAsync") {
                @Override
                public void run() {
                    try {
                        Solver s = (Solver) cs.newInstance();
                        StringBuilder text = new StringBuilder();
                        try {
                            s.solve(bundle, text);
                        } catch (Exception e) {
                            text.append("<code>AN EXCEPTION OCCURRED...\n");
                            StringWriter sw = new StringWriter();
                            e.printStackTrace(new PrintWriter(sw));
                            text.append(sw.toString());
                            text.append("</code>");
                        }
                        // If we got here, we won!
                        runOnUiThread(() -> {
                            Intent i = new Intent(StreamlineActivity.this, SolutionActivity.class);
                            i.putExtra(Intent.EXTRA_HTML_TEXT, text.toString());
                            startActivity(i);
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        });
    }
    public void bindRulesButton(String base) {
        findViewById(R.id.btn_rules).setOnClickListener((b) -> {
            Intent i = new Intent(this, SolutionActivity.class);
            i.putExtra(Intent.EXTRA_TITLE, getString(R.string.rules_title));
            i.putExtra("t20kdc.top.pipe", base);
            startActivity(i);
        });
    }
}
