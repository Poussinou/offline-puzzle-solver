package t20kdc.offlinepuzzlesolver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class WorkingActivity extends Activity {
    // If this thread is dead, the activity will be auto-closed.
    public static Thread currentWorkerThread;

    public static void launch(StreamlineActivity streamlineActivity, Thread thread) {
        currentWorkerThread = thread;
        thread.start();
        streamlineActivity.startActivity(new Intent(streamlineActivity, WorkingActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_working);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Nope. We're done.
        if ((currentWorkerThread == null) || (!currentWorkerThread.isAlive()))
            finish();
    }
}