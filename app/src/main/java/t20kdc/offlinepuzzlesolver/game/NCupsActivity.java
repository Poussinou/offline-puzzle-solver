package t20kdc.offlinepuzzlesolver.game;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import t20kdc.offlinepuzzlesolver.R;
import t20kdc.offlinepuzzlesolver.Streamline;
import t20kdc.offlinepuzzlesolver.StreamlineActivity;

public class NCupsActivity extends StreamlineActivity {
    public static final int MAX_CUPS = 20;
    private CheckBox[] cups = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_n_cups);
        cups = new CheckBox[MAX_CUPS];
        for (int i = 0; i < MAX_CUPS; i++) {
            CheckBox cb = new CheckBox(NCupsActivity.this);
            cb.setText("Cup " + (i + 1) + " Down");
            ((LinearLayout) findViewById(R.id.ll_cups)).addView(cb);
            cups[i] = cb;
        }
        ((SeekBar) findViewById(R.id.seek_cup_count)).setMax(MAX_CUPS - 1);
        ((SeekBar) findViewById(R.id.seek_cup_move)).setMax(MAX_CUPS - 1);
        loadSL(savedInstanceState);
        // these do an automatic update to account for loadSL above
        Streamline.bindSeekWdg(this, R.id.seek_cup_count, R.id.seek_cup_count_t, 1, new Runnable() {
            @Override
            public void run() {
                int p = ((SeekBar) findViewById(R.id.seek_cup_count)).getProgress() + 1;
                for (int i = 0; i < MAX_CUPS; i++)
                    cups[i].setVisibility(i < p ? View.VISIBLE : View.INVISIBLE);
            }
        });
        Streamline.bindSeekWdg(this, R.id.seek_cup_move, R.id.seek_cup_move_t, 1, null);
        bindRulesButton("file:///android_asset/rnc.html");
        bindSolveButton(NCupsSolver.class);
    }

    @Override
    public void onStreamline(Streamline streamline) {
        streamline.interact("seek_cup_count", R.id.seek_cup_count, 2);
        streamline.interact("seek_cup_move", R.id.seek_cup_move, 1);
        for (int i = 0; i < MAX_CUPS; i++)
            streamline.interact("cup_" + i, cups[i], false);
    }

}
