package t20kdc.offlinepuzzlesolver.game;

import android.os.Bundle;
import android.widget.SeekBar;

import t20kdc.offlinepuzzlesolver.R;
import t20kdc.offlinepuzzlesolver.Streamline;
import t20kdc.offlinepuzzlesolver.StreamlineActivity;

public class NQueensActivity extends StreamlineActivity {
    public static final int MAX_QUEENS = 32;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_n_queens);
        ((SeekBar) findViewById(R.id.seek_queens_size)).setMax(MAX_QUEENS - 1);
        loadSL(savedInstanceState);
        Streamline.bindSeekWdg(this, R.id.seek_queens_size, R.id.seek_queens_size_t, 1, () -> {
            // nothing (for now)
        });
        bindRulesButton("file:///android_asset/rnq.html");
        bindSolveButton(NQueensSolver.class);
    }

    @Override
    public void onStreamline(Streamline streamline) {
        streamline.interact("board_size", R.id.seek_queens_size, 7);
    }
}