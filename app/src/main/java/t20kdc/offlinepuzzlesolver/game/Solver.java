package t20kdc.offlinepuzzlesolver.game;

import android.os.Bundle;

public interface Solver {
    void solve(Bundle b, StringBuilder sol);
}
