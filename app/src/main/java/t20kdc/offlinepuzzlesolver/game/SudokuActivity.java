package t20kdc.offlinepuzzlesolver.game;

import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;

import t20kdc.offlinepuzzlesolver.R;
import t20kdc.offlinepuzzlesolver.Streamline;
import t20kdc.offlinepuzzlesolver.StreamlineActivity;

public class SudokuActivity extends StreamlineActivity {
    public int[] data = new int[9 * 9];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sudoku);
        loadSL(savedInstanceState);
        // Blatant cheating because of the zoom functionality
        WebView vw = findViewById(R.id.blatant_cheating);
        WebSettings ws = vw.getSettings();
        ws.setBuiltInZoomControls(true);
        ws.setJavaScriptEnabled(true);
        vw.addJavascriptInterface(new Object() {
            @JavascriptInterface
            public int getCell(int i) {
                return data[i];
            }
            @JavascriptInterface
            public void setCell(int i, int v) {
                data[i] = v;
            }
        }, "Towertop");
        vw.loadUrl("file:///android_asset/sudoku_editor.html");
        bindRulesButton("file:///android_asset/rsu.html");
        bindSolveButton(SudokuSolver.class);
    }

    @Override
    public void onStreamline(Streamline streamline) {
        for (int i = 0; i < 9 * 9; i++) {
            if (streamline.saving) {
                streamline.putInt("c" + i, data[i]);
            } else {
                data[i] = streamline.getInt("c" + i, 0);
            }
        }
    }
}