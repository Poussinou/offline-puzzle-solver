package t20kdc.offlinepuzzlesolver.game;

import android.os.Bundle;

import java.util.Arrays;
import java.util.Collection;

import t20kdc.offlinepuzzlesolver.genlog.TreeWalker;
import t20kdc.offlinepuzzlesolver.genlog.TreeWalkerRoot;

/**
 * Fun fact.
 * This class used to be nice and logical and also efficient.
 * BUT THEN IT DIDN'T WORK.
 */
public class SudokuSolver implements Solver {
    private static final int[] squareOfs = {
            0, 1, 2,
            9, 10, 11,
            18, 19, 20
    };
    @Override
    public void solve(Bundle b, StringBuilder sol) {
        int[] choices = new int[9 * 9];
        Arrays.fill(choices, 9);
        // Load state...
        State st = new State();
        for (int i = 0; i < 9 * 9; i++) {
            int v = b.getInt("c" + i);
            if (v != 0) {
                if (st.wouldBeValid(i, v - 1)) {
                    st = new State(st, i, v - 1);
                } else {
                    sol.append("<i>The input contained a contradiction in tile " + ((i % 9) + 1) + ", " + ((i / 9) + 1) + ".</i>");
                    return;
                }
            }
        }
        // Run
        TreeWalker<State> tw = TreeWalker.of(new TreeWalkerRoot<State>() {
            @Override
            public void initState(State init) {
            }
            @Override
            public void visit(State node, Collection<State> intern) {
                int bestVisit = -1;
                int bestVisitChoices = 10;
                // in case of error, lower maximum choices so it completes in reasonable time,
                // but 'allow it to continue' past errors
                boolean walkaboutsLockout = false;
                int walkaboutsCounter = 1;
                for (int i = 0; i < 9 * 9; i++) {
                    if (node.completion[i])
                        continue;
                    int c = node.getChoiceCount(i);
                    if (node.enableWalkabouts && (c == 0)) {
                        walkaboutsLockout = true;
                        continue;
                    }
                    if (c < bestVisitChoices) {
                        bestVisit = i;
                        bestVisitChoices = c;
                    }
                }
                if (bestVisit == -1)
                    return;
                if (bestVisitChoices == 0)
                    return;
                for (int i = 0; i < 9; i++) {
                    if (node.wouldBeValid(bestVisit, i)) {
                        intern.add(new State(node, bestVisit, i));
                        if (walkaboutsLockout)
                            if (--walkaboutsCounter <= 0)
                                return;
                    }
                }
            }
            @Override
            public int getImportanceOf(State node) {
                return node.completionAmount;
            }
            @Override
            public boolean isEndpoint(State node) {
                return node.completionAmount == 9 * 9;
            }
        });
        State res = tw.walk(st);
        if (res == null) {
            sol.append("<p>Failed.</p>");
            sol.append("<p>But here's the best shot anyway...</p>");
            st.enableWalkabouts = true;
            tw.walk(st);
            res = tw.getHighestImportance();
            sol.append("<h1>Closest</h1>");
        } else {
            sol.append("<h1>Solution</h1>");
        }
        if (res != null) {
            sol.append("<h2>Main</h2>");
            sol.append("<table border=\"1\" style=\"font-family: monospace;\">");
            for (int y = 0; y < 9; y++) {
                if ((y != 0) && ((y % 3) == 0))
                    sol.append("<tr><td></td></tr>");
                sol.append("<tr>");
                for (int x = 0; x < 9; x++) {
                    if ((x % 3) == 0)
                        sol.append("<td></td>");
                    char ch = '?';
                    int choice = res.getChoice(x + (y * 9));
                    if (choice != -1) {
                        ch = (char) ('1' + choice);
                        sol.append("<td>");
                    } else {
                        sol.append("<td style=\"color: transparent;\">");
                    }
                    sol.append(ch);
                    sol.append("</td>");
                }
                sol.append("</tr>");
            }
            sol.append("</table>");
            sol.append("<h2>Formatted for copying</h2>");
            sol.append("<pre>\n");
            for (int y = 0; y < 9; y++) {
                if ((y % 3) == 0)
                    sol.append('\n');
                for (int x = 0; x < 9; x++) {
                    if ((x % 3) == 0)
                        sol.append(' ');
                    char ch = '?';
                    int choice = res.getChoice(x + (y * 9));
                    if (choice != -1)
                        ch = (char) ('1' + choice);
                    sol.append(ch);
                }
                sol.append('\n');
            }
            sol.append("</pre>\n");
        }
    }
    public static class State {
        public final boolean[] validity;
        public final boolean[] completion;
        public final int completionAmount;
        public boolean enableWalkabouts = false;
        public State() {
            validity = new boolean[9 * 9 * 9];
            completion = new boolean[9 * 9];
            Arrays.fill(validity, true);
            completionAmount = 0;
        }
        public State(State previous, int where, int choice) {
            enableWalkabouts = previous.enableWalkabouts;
            validity = previous.validity.clone();
            completion = previous.completion.clone();
            if (completion[where])
                throw new RuntimeException("double-completion");
            if ((choice < 0) || (choice > 8))
                throw new RuntimeException("oor choice " + where + " : " + choice);
            if (!validity[(where * 9) + choice])
                throw new RuntimeException("invalid choice");
            // Alright, firstly, disable all validity on this square
            for (int i = 0; i < 9; i++)
                validity[(where * 9) + i] = false;
            // Then propagate non-validity to other squares
            int whereX = where % 9;
            int whereY = where / 9;
            int wcXB = (whereX * 9) + choice;
            int wcYB = (whereY * 9 * 9) + choice;
            int wSB = ((whereX / 3) * 3) + ((whereY / 3) * 3 * 9);
            int wcSB = (wSB * 9) + choice;
            for (int i = 0; i < 9; i++) {
                validity[wcXB + (i * 9 * 9)] = false;
                validity[wcYB + (i * 9)] = false;
                validity[wcSB + (squareOfs[i] * 9)] = false;
            }
            // Finally, re-enable validity on this square
            validity[(where * 9) + choice] = true;
            completion[where] = true;
            completionAmount = previous.completionAmount + 1;
        }
        public boolean wouldBeValid(int where, int choice) {
            return validity[(where * 9) + choice];
        }
        public int getChoiceCount(int where) {
            int c = 0;
            for (int i = 0; i < 9; i++)
                if (wouldBeValid(where, i))
                    c++;
            return c;
        }
        public int getChoice(int where) {
            int c = -1;
            for (int i = 0; i < 9; i++) {
                if (wouldBeValid(where, i)) {
                    if (c != -1)
                        return -1;
                    c = i;
                }
            }
            return c;
        }
    }
}
