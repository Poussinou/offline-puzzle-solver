package t20kdc.offlinepuzzlesolver.genlog;

import java.lang.reflect.Array;
import java.util.LinkedList;

/**
 * Cheats to shut up stupid warnings
 */
public final class GA {
    private GA() {
    }
    public static <T> T[] of(Object o) {
        return (T[]) o;
    }
    public static <T> T[] ofLL(LinkedList<T> ll, Class<?> c) {
        return ll.toArray((T[]) Array.newInstance(c, ll.size()));
    }
}
