package t20kdc.offlinepuzzlesolver.genlog;

public interface TreeWalker<N> {
    static <N> TreeWalker<N> of(TreeWalkerRoot<N> stateTreeWalkerRoot) {
        return new TreeWalkerThreads<>(stateTreeWalkerRoot);
    }

    N walk(N init);

    N getHighestImportance();
}
