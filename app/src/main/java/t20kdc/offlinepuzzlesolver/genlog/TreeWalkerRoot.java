package t20kdc.offlinepuzzlesolver.genlog;

import java.util.Collection;

/**
 * Uuuugh
 */
public interface TreeWalkerRoot<N> {
    /**
     * Initializes state.
     * @param init node
     */
    void initState(N init);

    /**
     * Visits the node. Note that this is allowed to mutate the node in-place and then put it back on the list.
     * @param intern Results are written in here.
     */
    void visit(N node, Collection<N> intern);

    /**
     * @return Importance of this node. A node *roughly* of the highest importance is kept around.
     */
    int getImportanceOf(N node);

    /**
     * @return If the node is an endpoint (pathfinding ought to stop here).
     */
    boolean isEndpoint(N node);
}
