package t20kdc.offlinepuzzlesolver.genlog;

import java.util.ArrayList;

/**
 * The simplest possible tree walker.
 */
public class TreeWalkerSimple<N> implements TreeWalker<N> {
    private final TreeWalkerRoot<N> root;
    private N highestImportance;

    public TreeWalkerSimple(TreeWalkerRoot<N> r) {
        root = r;
    }

    @Override
    public N walk(N init) {
        root.initState(init);
        highestImportance = null;
        return walkI(init);
    }

    private N walkI(N init) {
        if (root.isEndpoint(init))
            return init;
        if (highestImportance == null) {
            highestImportance = init;
        } else if (root.getImportanceOf(highestImportance) < root.getImportanceOf(init)) {
            highestImportance = init;
        }
        ArrayList<N> al = new ArrayList<>(16);
        root.visit(init, al);
        for (N v : al) {
             N res = walkI(v);
             if (res != null)
                 return res;
        }
        return null;
    }

    @Override
    public N getHighestImportance() {
        return highestImportance;
    }
}
