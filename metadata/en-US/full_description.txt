Offline Puzzle Solver solves various kinds of puzzles:

- Sudoku
- Three/N-Cups problem
- 8/N-Queens problem
- The Challenge puzzle in the NetGuard firewall (as of current versions of NetGuard at time of writing. subject to change.)

